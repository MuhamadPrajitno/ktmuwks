<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>KTM UWKS | <?= $title ?></title>
	<link rel="shorcut icon" href="<?= base_url('assets/img/logo-uwks.png') ?>">

	<!-- css lib -->
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/bootstrap-flatly.min.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/font-awesome-4.7.0.min.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/main_styles.css') ?>">

	<!-- js lib -->
	<script type="text/javascript" src="<?= base_url('assets/js/jquery-3.1.1.min.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('assets/js/main.js') ?>"></script>

</head>
<body>

	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">
					<img class="img-responsive navbar-logo" src="<?= base_url('assets/img/logo-uwks.png') ?>">
				</a>
			</div> <!-- navbar-header -->

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					
				</ul> <!-- navbar-right -->
			</div> <!-- navbar-collapse -->

		</div> <!-- container/container-fluid -->
	</nav> <!-- navbar navbar-default -->