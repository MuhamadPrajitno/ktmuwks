<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rec extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$data = [
			'title'				=> 'Rekam KTM Mahasiswa',
			'content'			=> 'rec/index',
		];

		$this->load->view('template', $data);
	}

}

/* End of file Rec.php */
/* Location: ./application/controllers/Rec.php */